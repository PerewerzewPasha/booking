<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomsTabel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('room_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('rooms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('capacity');
            $table->unsignedInteger('category_id');
            $table->unsignedInteger('hotel_id');
            $table->timestamps();

            $table->foreign('category_id')->references('id')->on('room_categories')
                ->onUpdate('cascade');

            $table->foreign('hotel_id')->references('id')->on('hotels')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('rooms', function (Blueprint $table) {
            $table->dropForeign('rooms_category_id_foreign');
            $table->dropForeign('rooms_hotel_id_foreign');
        });


        Schema::dropIfExists('rooms');
        Schema::dropIfExists('room_categories');
    }
}
