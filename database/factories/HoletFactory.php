<?php

use App\Models\RoomCategories;
use Faker\Generator as Faker;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->defineAs(\App\Models\Hotel::class, 'hotel', function (Faker $faker) {
    return [
        'name' => 'Гостиница "'.$faker->firstName('female').'"',
        'rating' => $faker->numberBetween(1,5)
    ];
});


$factory->defineAs(\App\Models\Room::class, 'room', function (Faker $faker) {
    return [
        'name' => $faker->firstName('male')." №".$faker->randomNumber(),
        'category_id' => $faker->randomElement(RoomCategories::all('id')->pluck('id')->toArray()),
        'capacity' => $faker->numberBetween(1,10)
    ];
});
