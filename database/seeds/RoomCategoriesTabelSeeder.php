<?php

use App\Models\RoomCategories;
use Illuminate\Database\Seeder;

class RoomCategoriesTabelSeeder extends Seeder
{

    private $category = [
        1 => 'Стандарт',
        2 => 'Полулюкс',
        3 => 'Делюкс',
        4 => 'Люкс',
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->category as $id => $name) {
            if (RoomCategories::find($id) == null) {
                RoomCategories::create([
                    'name' => $name,
                ]);
            }
        }
    }
}
