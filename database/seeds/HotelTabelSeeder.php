<?php

use App\Models\Hotel;
use App\Models\Room;
use Illuminate\Database\Seeder;

class HotelTabelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Hotel::class, 'hotel', 10)->create()->each(function ($hotel){
            /** @var Hotel $hotel */
            $hotel->rooms()->saveMany(factory(Room::class, 'room', 10)->make());
        });
    }
}
