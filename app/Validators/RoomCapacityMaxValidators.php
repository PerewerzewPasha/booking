<?php


namespace App\Validators;


use App\Models\Room;

class RoomCapacityMaxValidators
{
    public function validate(string $attribute, $value, array $parameters): bool
    {

        list($hotel_field, $room_field) = $parameters;

        if (!is_numeric($value) || !request()->has($hotel_field) || !request()->has($room_field)) {
            return false;
        }



        $room = Room::where('hotel_id', request()->get($hotel_field))->findOrFail(request()->get($room_field));

        if ($room->capacity < $value) {
            return false;
        } else {
            return true;
        }
    }
}