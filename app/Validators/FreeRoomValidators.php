<?php

namespace App\Validators;


use App\Models\Room;

class FreeRoomValidators
{
    public function validate(string $attribute, $value, array $parameters): bool
    {

        list($room_field) = $parameters;

        if (strtotime($value) === false || !request()->has($room_field)) {
            return false;
        }

             return Room::freeDate($value)->find(request()->get($room_field)) ? true : false;
    }
}