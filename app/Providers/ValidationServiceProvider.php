<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ValidationServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerValidationRules($this->app['validator']);
    }

    /**
     * @param \Illuminate\Contracts\Validation\Factory $validator
     */
    protected function registerValidationRules(\Illuminate\Contracts\Validation\Factory $validator)
    {
        $validator->extend('room_capacity_max', 'App\Validators\RoomCapacityMaxValidators@validate', 'The field is incorrect.');
        $validator->extend('free_room', 'App\Validators\FreeRoomValidators@validate', 'The room is occupied on these dates.');
    }
}