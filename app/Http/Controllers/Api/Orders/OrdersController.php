<?php

namespace App\Http\Controllers\Api\Orders;


use App\Http\Controllers\Controller;
use App\Http\Resources\OrderResource;
use App\Models\Order;
use App\Models\Room;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\DB;

class OrdersController extends Controller
{


    public function index(Order $order) : AnonymousResourceCollection
    {
        return OrderResource::collection($order::paginate(request()->get('per_page'))->appends(request()->all()));
    }

    public function show(Order $order) : OrderResource
    {
        return new OrderResource($order);
    }


    public function store()
    {
        $this->validate(request(), [
            'hotel_id' => 'required|integer|exists:hotels,id',
            'room_id' => 'required|integer|exists:rooms,id',
            'client_name' => 'required',
            'client_phone' => 'required',
            'arrival_date' => 'required|date|before:departure_date|free_room:room_id',
            'departure_date' => 'required|date|after:arrival_date|free_room:room_id',
            'number_people' => 'required|integer|bail|room_capacity_max:hotel_id,room_id',
        ]);

        /** @var Room $room */
        $room = Room::where('hotel_id', request()->get('hotel_id'))->findOrFail(request()->get('room_id'));

        DB::beginTransaction();
        try {
            /** @var Order $order */
            $order = $room->orders()->make(request()->only(['client_name', 'client_phone', 'arrival_date', 'departure_date', 'number_people']));
            $order->hotel()->associate($room->hotel);

            if (auth()->user()) {
                $order->user()->associate(auth()->user());
            }
            $order->save();
            DB::commit();
            return new OrderResource($order);
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json(['message' => $exception->getMessage(), 'status' => 503], 503);
        }
    }

    public function destroy(Order $order) : JsonResponse
    {
        DB::beginTransaction();

        try {
            $order->delete();
            DB::commit();
            return response()->json(['message' => 'The order was successfully deleted', 'status' => 200]);
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json(['message' => $exception->getMessage(), 'status' => 503], 503);
        }
    }

}