<?php

namespace App\Http\Controllers\Api\Hotels;

use App\Http\Resources\HotelResource;
use App\Models\Hotel;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\DB;

class HotelsController extends Controller
{

    public function index(Hotel $hotel) : AnonymousResourceCollection
    {
        return HotelResource::collection($hotel::rating(request()->get('rating'))->paginate(request()->get('per_page'))->appends(request()->all()));
    }

    public function store()
    {
        $this->validate(request(), [
            'name' => 'required',
            'rating' => 'required|in:' . implode(',', Hotel::RATING),
        ]);

        DB::beginTransaction();

        try {
            $attributes = [
                'name' => request()->get('name'),
                'rating' => request()->get('rating'),
            ];

            $hotel = Hotel::create($attributes);
            DB::commit();
            return new HotelResource($hotel);
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json(['message' => $exception->getMessage(), 'status' => 503]);
        }
    }

    public function show(Hotel $hotel): HotelResource
    {
        return new HotelResource($hotel);
    }

    public function update(Hotel $hotel)
    {
        $this->validate(request(), [
            'rating' => 'in:' . implode(',', Hotel::RATING),
        ]);

        DB::beginTransaction();

        try {
            $attributes = [
                'name' => request()->get('name', $hotel->name),
                'rating' => request()->get('rating', $hotel->rating),
            ];

            $hotel->update($attributes);
            DB::commit();

            return new HotelResource($hotel);
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json(['message' => $exception->getMessage(), 'status' => 503], 503);
        }
    }

    public function destroy(Hotel $hotel) : JsonResponse
    {
        DB::beginTransaction();

        try {
            $hotel->delete();
            DB::commit();
            return response()->json(['message' => 'The hotel was successfully deleted', 'status' => 200]);
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json(['message' => $exception->getMessage(), 'status' => 503], 503);
        }
    }

    public function edit(Hotel $hotel)
    {

    }

}
