<?php

namespace App\Http\Controllers\Api\Hotels;


use App\Http\Controllers\Controller;
use App\Http\Resources\RoomResource;
use App\Models\Hotel;
use App\Models\Room;
use App\Models\RoomCategories;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

class RoomsController extends Controller
{

    public function getAll(Room $room)
    {
    	$this->validate(request(), [
    		'hotel_id' => 'sometimes|integer|exists:hotels,id',
		    'category_id' => 'sometimes|exists:room_categories,id',
		    'arrival_date' => 'sometimes|date',
		    'departure_date' => 'sometimes|date',
		    'capacity' => 'sometimes|integer',
	    ]);

        $rooms = $room::capacity(request()->get('capacity'))
            ->hotel(request()->get('hotel_id'))
            ->category(request()->get('category_id'))
            ->freeDate(request()->get('arrival_date'), request()->get('departure_date'))
            ->paginate(request()->get('per_page'))
            ->appends(request()->all());

        return RoomResource::collection($rooms);
    }

    public function index(Hotel $hotel): AnonymousResourceCollection
    {
        return RoomResource::collection($hotel->rooms()->paginate(request()->get('per_page'))->appends(request()->all()));
    }


    public function store(Hotel $hotel)
    {
        $this->validate(request(), [
            'name' => 'required',
            'capacity' => 'required|integer',
            'category_id' => 'required|exists:room_categories,id',
        ]);


        DB::beginTransaction();

        try {
            $attributes = [
                'name' => request()->get('name'),
                'capacity' => request()->get('capacity'),
            ];


            $room = $hotel->rooms()->make($attributes);
            $room->category()->associate(RoomCategories::findOrFail(request()->get('category_id')));
            $room->save();
            DB::commit();
            return new RoomResource($room);
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json(['message' => $exception->getMessage(), 'status' => 503]);
        }
    }

    /**
     * @param Hotel $hotel
     * @param Room $room
     * @return RoomResource|JsonResponse
     */
    public function update(Hotel $hotel, Room $room)
    {
        $this->validate(request(), [
            'capacity' => 'sometimes|integer',
            'category_id' => 'sometimes|required|integer|exists:room_categories,id',
        ]);


        DB::beginTransaction();

        try {

            /** @var Room $room */
            $room = $hotel->rooms()->findOrFail($room->getKey());

            $attributes = [
                'name' => request()->get('name', $room->name),
                'capacity' => request()->get('capacity', $room->capacity),
            ];

            $room->category()->associate(RoomCategories::findOrFail(request()->get('category_id', $room->category_id)));
            $room->update($attributes);
            DB::commit();

            return new RoomResource($room);
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json(['message' => $exception->getMessage(), 'status' => 503]);
        }
    }

    public function show(Hotel $hotel, Room $room): RoomResource
    {
        return new RoomResource($hotel->rooms()->findOrFail($room->getKey()));
    }


    public function destroy(Hotel $hotel, Room $room): JsonResponse
    {
        DB::beginTransaction();

        try {
            $hotel->rooms()->findOrFail($room->getKey())->delete();
            DB::commit();
            return response()->json(['message' => 'The room was successfully deleted', 'status' => 200]);
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json(['message' => $exception->getMessage(), 'status' => 503], 503);
        }
    }

}