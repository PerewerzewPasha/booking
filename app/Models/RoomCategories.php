<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class RoomCategories extends Model
{
    protected $fillable = [
        'name'
    ];

    public $timestamps = true;

    public function rooms(): HasMany
    {
        return $this->hasMany(Room::class);
    }
}
