<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Hotel extends Model
{
    const RATING = [1,2,3,4,5];

    protected $fillable = [
        'name',
        'rating'
    ];

    public $timestamps = true;

    public function rooms(): HasMany
    {
        return $this->hasMany(Room::class, 'hotel_id', 'id');
    }

    public function scopeRating(Builder $query, int $rating = null)
    {
        if (!is_null($rating)) {
            return $query->where('rating', $rating);
        } else {
            return $query;
        }
    }
}
