<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Room extends Model
{
    protected $fillable = [
        'name',
        'capacity',
    ];

    protected $casts = [
        'capacity' => 'int',
    ];

    public $timestamps = true;

    public function hotel(): BelongsTo
    {
        return $this->belongsTo(Hotel::class, 'hotel_id');
    }

    public function orders(): HasMany
    {
        return $this->hasMany(Order::class, 'room_id');
    }

    public function category(): BelongsTo
    {
        return $this->belongsTo(RoomCategories::class);
    }

    public function scopeCapacity(Builder $query, int $capacity = null)
    {
        if ($capacity != null) {
            return $query->where('capacity', $capacity);
        } else {
            return $query;
        }
    }

    public function scopeHotel(Builder $query, int $hotel = null)
    {
        if ($hotel != null) {
            return $query->whereHas('hotel', function ($query) use ($hotel) {
                return $query->where('id', $hotel);
            });
        } else {
            return $query;
        }
    }

    public function scopeFreeDate(Builder $query, $arrival_date = null, $departure_date = null)
    {
        if (strtotime($arrival_date) !== false || strtotime($departure_date) !== false) {
            $query->whereDoesntHave('orders', function (Builder $query) use ($arrival_date, $departure_date) {

                if($arrival_date != null){
                    $query->whereRaw('? BETWEEN arrival_date and departure_date', [$arrival_date]);
                }

                if($departure_date != null){
                    $query->whereRaw('? BETWEEN arrival_date and departure_date', [$departure_date]);
                }

                return $query;
            });

        } else {
            return $query;
        }
    }

    public function scopeCategory(Builder $query, int $category = null)
    {
        if ($category != null) {
            return $query->whereHas('category', function ($query) use ($category) {
                return $query->where('id', $category);
            });
        } else {
            return $query;
        }
    }


}
